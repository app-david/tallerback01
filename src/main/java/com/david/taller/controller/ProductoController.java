package com.david.taller.controller;

import com.david.taller.model.ProductoModel;
import com.david.taller.model.ProductoPrecioOnly;
import com.david.taller.services.ProductoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/${version}/productos")
public class ProductoController {

    @Autowired
    private ProductoService productService;

    @GetMapping
    public ResponseEntity listProducto() {
        List<ProductoModel> list = productService.listProducto();
        if(list == null || list.isEmpty()) {
            return new ResponseEntity<>(list, HttpStatus.NO_CONTENT);
        } else{
            return new ResponseEntity<>(list, HttpStatus.OK);
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity getProductoId(@PathVariable int id) {
        ProductoModel pr = productService.getProducto(id);
        if (pr == null) {
            return new ResponseEntity<>("Producto no encontrado.", HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok(pr);
    }

    @PostMapping
    public ResponseEntity<String> addProducto(@RequestBody ProductoModel productoModel) {
        productService.addProducto(productoModel);
        return new ResponseEntity<>("Producto creado correctamente!", HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity updateProducto(@PathVariable int id, @RequestBody ProductoModel productToUpdate) {
        ProductoModel pr = productService.getProducto(id);
        if (pr == null) {
            return new ResponseEntity<>("Producto no encontrado.", HttpStatus.NOT_FOUND);
        }
        productService.updateProducto(id, productToUpdate);
        return new ResponseEntity<>("Producto actualizado correctamente.", HttpStatus.NO_CONTENT);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity deleteProducto(@PathVariable Integer id) {
        ProductoModel pr = productService.getProducto(id);
        if(pr == null) {
            return new ResponseEntity<>("Producto no encontrado.", HttpStatus.NOT_FOUND);
        }
        productService.removeProducto(id);
        return new ResponseEntity<>("Producto eliminado correctamente.", HttpStatus.NO_CONTENT);
    }

    @PatchMapping("/{id}")
    public ResponseEntity patchPrecioProducto(@RequestBody ProductoPrecioOnly productoPrecioOnly, @PathVariable int id){
        ProductoModel pr = productService.getProducto(id);
        if (pr == null) {
            return new ResponseEntity<>("Producto no encontrado.", HttpStatus.NOT_FOUND);
        }
        if(productoPrecioOnly.getPrecio() == null){
            return new ResponseEntity<>("Parámetros incorrectos.", HttpStatus.BAD_REQUEST);
        }
        pr.setPrecio(productoPrecioOnly.getPrecio());
        productService.updateProducto(id, pr);
        return new ResponseEntity<>(pr, HttpStatus.OK);
    }
}
