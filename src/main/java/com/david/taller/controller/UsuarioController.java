package com.david.taller.controller;

import com.david.taller.model.ProductoModel;
import com.david.taller.model.UsuarioModel;
import com.david.taller.services.ProductoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/${version}/productos")
public class UsuarioController {

    @Autowired
    private ProductoService productService;

    @GetMapping("/{id}/users")
    public ResponseEntity listUsuarios(@PathVariable int id) {
        ProductoModel pr = productService.getProducto(id);
        if (pr == null) {
            return new ResponseEntity<>("Producto no encontrado.", HttpStatus.NOT_FOUND);
        }
        if (pr.getUsers() != null && !pr.getUsers().isEmpty())
            return ResponseEntity.ok(pr.getUsers());
        else
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @GetMapping("/{idProducto}/users/{idUsuario}")
    public ResponseEntity getUsuario(@PathVariable int idProducto, @PathVariable int idUsuario) {
        ProductoModel pr = productService.getProducto(idProducto);
        if (pr == null) {
            return new ResponseEntity<>("Producto no encontrado.", HttpStatus.NOT_FOUND);
        }
        UsuarioModel usu = productService.getUsuario(idProducto, idUsuario);
        if (usu == null) {
            return new ResponseEntity<>("Usuario no encontrado.", HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok(usu);
    }

    @PostMapping("/{id}/users")
    public ResponseEntity<String> addUser(@PathVariable int id, @RequestBody UsuarioModel usuario) {
        ProductoModel pr = productService.getProducto(id);
        if (pr == null) {
            return new ResponseEntity<>("Producto no encontrado.", HttpStatus.NOT_FOUND);
        }
        productService.addUsuario(id, usuario);
        return new ResponseEntity<>("Usuario creado correctamente!", HttpStatus.CREATED);
    }

    @PutMapping("/{idProducto}/users/{idUsuario}")
    public ResponseEntity updateUsuario(@PathVariable int idProducto, @PathVariable int idUsuario,
                                        @RequestBody UsuarioModel userToUpdate) {
        ProductoModel pr = productService.getProducto(idProducto);
        if (pr == null) {
            return new ResponseEntity<>("Producto no encontrado.", HttpStatus.NOT_FOUND);
        }
        UsuarioModel usu = productService.getUsuario(idProducto, idUsuario);
        if (usu == null) {
            return new ResponseEntity<>("Usuario no encontrado.", HttpStatus.NOT_FOUND);
        }
        productService.updateUsuario(idProducto, idUsuario, userToUpdate);
        return new ResponseEntity<>("Usuario actualizado correctamente.", HttpStatus.NO_CONTENT);
    }

    @DeleteMapping("/{idProducto}/users/{idUsuario}")
    public ResponseEntity deleteProducto(@PathVariable int idProducto, @PathVariable int idUsuario) {
        ProductoModel pr = productService.getProducto(idProducto);
        if(pr == null) {
            return new ResponseEntity<>("Producto no encontrado.", HttpStatus.NOT_FOUND);
        }
        UsuarioModel usu = productService.getUsuario(idProducto, idUsuario);
        if (usu == null) {
            return new ResponseEntity<>("Usuario no encontrado.", HttpStatus.NOT_FOUND);
        }
        productService.removeUsuario(idProducto, idUsuario);
        return new ResponseEntity<>("Usuario eliminado correctamente.", HttpStatus.NO_CONTENT);
    }
}
