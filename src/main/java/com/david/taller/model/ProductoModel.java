package com.david.taller.model;

import java.util.List;

public class ProductoModel {

    private int id;
    private String nombre;
    private double precio;
    private List<UsuarioModel> users;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public List<UsuarioModel> getUsers() {
        return users;
    }

    public void setUsers(List<UsuarioModel> users) {
        this.users = users;
    }
}
