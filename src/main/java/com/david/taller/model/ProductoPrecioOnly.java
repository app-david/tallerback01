package com.david.taller.model;

public class ProductoPrecioOnly {

    private Double precio;

    public Double getPrecio() {
        return precio;
    }

    public void setPrecio(Double precio) {
        this.precio = precio;
    }
}
