package com.david.taller.services;

import com.david.taller.model.ProductoModel;
import com.david.taller.model.UsuarioModel;

import java.util.List;

public interface ProductoService {

    List<ProductoModel> listProducto();
    ProductoModel getProducto(int id);
    int addProducto(ProductoModel producto);
    void updateProducto(int id, ProductoModel producto);
    void removeProducto(int id);
    int addUsuario(int idProducto, UsuarioModel usuario);
    UsuarioModel getUsuario(int idProducto, int idUsuario);
    void updateUsuario(int idProducto, int idUsuario, UsuarioModel usuario);
    void removeUsuario(int idProducto, int idUsuario);
}
