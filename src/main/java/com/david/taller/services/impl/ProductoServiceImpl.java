package com.david.taller.services.impl;

import com.david.taller.model.ProductoModel;
import com.david.taller.model.UsuarioModel;
import com.david.taller.services.ProductoService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Predicate;

@Service
public class ProductoServiceImpl implements ProductoService {

    //Simulamos la persistencia de datos en una Lista global
    private List<ProductoModel> listProductos = new ArrayList<>();

    private AtomicInteger sequenceProducto = new AtomicInteger(1);
    private AtomicInteger sequenceUsuario = new AtomicInteger(1);

    @Override
    public List<ProductoModel> listProducto() {
        return listProductos;
    }

    @Override
    public ProductoModel getProducto(int id) {
        Optional<ProductoModel> find = listProductos.stream().filter(p -> p.getId() == id).findFirst();
        return find.isEmpty() ? null : find.get();
    }

    @Override
    public int addProducto(ProductoModel producto) {
        //Generamos un ID único e irrepetible según la fecha actual
        producto.setId(sequenceProducto.getAndIncrement());
        listProductos.add(producto);
        return producto.getId();
    }

    @Override
    public void updateProducto(int id, ProductoModel productoUpdate) {
        for( int i = 0 ; i < listProductos.size(); i++ ){
            if(listProductos.get(i).getId() == id){
                productoUpdate.setId(id);
                listProductos.set(i, productoUpdate);
                break;
            }
        }
    }

    @Override
    public void removeProducto(int id) {
        for( int i = 0 ; i < listProductos.size(); i++ ){
            if(listProductos.get(i).getId() == id){
                listProductos.remove(i);
                break;
            }
        }
    }

    @Override
    public int addUsuario(int idProducto, UsuarioModel usuario) {
        ProductoModel producto = getProducto(idProducto);

        if(producto.getUsers() == null) producto.setUsers(new ArrayList<>());

        usuario.setId(sequenceUsuario.getAndIncrement());
        producto.getUsers().add(usuario);
        return usuario.getId();
    }

    @Override
    public UsuarioModel getUsuario(int idProducto, int idUsuario) {
        ProductoModel producto = getProducto(idProducto);
        if(producto != null && producto.getUsers() != null){
            Optional<UsuarioModel> find = producto.getUsers().stream().filter(u -> u.getId() == idUsuario).findFirst();
            return find.isEmpty() ? null : find.get();
        }
        return null;
    }

    @Override
    public void updateUsuario(int idProducto, int idUsuario, UsuarioModel usuarioToUpdate) {
        UsuarioModel usuarioOriginal = getUsuario(idProducto, idUsuario);
        if(usuarioOriginal != null) {
            //no tenemos más campos que actualizar, se actualiza por referencia
            usuarioOriginal.setNombre(usuarioToUpdate.getNombre());
        }
    }

    @Override
    public void removeUsuario(int idProducto, int idUsuario) {
        ProductoModel producto = getProducto(idProducto);
        if(producto != null && producto.getUsers() != null){
            for( int i = 0 ; i < producto.getUsers().size(); i++ ){
                if(producto.getUsers().get(i).getId() == idUsuario){
                    producto.getUsers().remove(i);
                    break;
                }
            }
        }
    }
}
